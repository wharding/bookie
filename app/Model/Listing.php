<?php
App::uses('AppModel', 'Model');
/**
 * Listing Model
 *
 * @property User $User
 * @property Item $Item
 * @property Upload $Upload
 */
class Listing extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';
	
    public $actsAs = array(
		'Search.Searchable',//allow searching via the search plugin
	);
	
	//config for search plugin
	public $filterArgs = array(
		'search' => array('type' => 'query', 'method' => 'combinedSearch'),
		// 'id' => array('type' => 'value', 'allowEmpty' => true),
		// 'title' => array('type' => 'like', 'allowEmpty' => true),
		// 'subject' => array('type' => 'value', 'allowEmpty' => true),
		// 'description' => array('type' => 'like', 'allowEmpty' => true),
		// 'creator' => array('type' => 'like', 'allowEmpty' => true),
		// 'publisher' => array('type' => 'like', 'allowEmpty' => true),
		// 'contributor' => array('type' => 'like', 'allowEmpty' => true),
		// 'lang' => array('type' => 'value', 'allowEmpty' => true),
		// 'rights' => array('type' => 'like', 'allowEmpty' => true)
	);

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'id' => array(
			'numeric' => array(
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'user_id' => array(
		//
			//'numeric' => array(
				//'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			//),
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'title' => array(
			'maxlength' => array(
				'rule' => array('maxlength',200),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'price' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Item' => array(
			'className' => 'Item',
			'foreignKey' => 'listing_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Image' => array(
			'className' => 'Upload',
			'foreignKey' => 'foreign_key',
			'dependent' => false,
			'conditions' => array(
				'Image.model' => 'Listing'
			),
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
    public function afterFind($results, $primary = false){
    	//the public facing directory for images
		$public_base_dir = '/files/image/upload/';
		//the internal file system location of the image
		$private_base_dir = WWW_ROOT . 'files' . DS . 'image' . DS . 'upload';
        foreach($results as &$result){
			//add a field to all listings to determine is the current user is the owner
			//
	        //this field also acts as a check for editign and deleting items, since only listing owners (or admins)
	        //can add items to a listing
			if(isset($result['User']['id']) && isset($_SESSION['Auth']['User']['id'])) {
				$result['Listing']['isOwner'] = ($_SESSION['Auth']['User']['id'] == $result['User']['id']);
			} else {
				$result['Listing']['isOwner'] = false;
			}
			//update the record with the public image reference
			$file_pointer = $private_base_dir . '/' . @$result['Image'][0]['id'] . '/' . 'thumb_' . @$result['Image'][0]['upload'];
			$file_pointer = Folder::correctSlashFor($file_pointer);
			
			if(isset($result['Image'][0]) && file_exists($file_pointer)) {
				$this->log('Image exists', 'debug');
				$result['Listing']['thumb'] = $public_base_dir . $result['Image'][0]['id'] . '/thumb_' . $result['Image'][0]['upload'];
			} else {
				$result['Listing']['thumb'] = '/img/thumb_no_preview.jpg';
			}
		}
		unset($result);
		return $results;
	}
	
	public function combinedSearch($data = array()){
		$search = $data['search'];
		// $this->Listing->Behaviors->load('Containable');
		$cond = array(
			'OR' => array(
				'Listing.title LIKE' => '%' . $search . '%',
				'Listing.description LIKE' => '%' . $search . '%',
				// 'Item.title LIKE' => '%' . $search . '%',
				// 'Item.description LIKE' => '%' . $search . '%',
				// 'Item.author LIKE' => '%' . $search . '%',
				// 'Item.isbn_10 LIKE' => '%' . $search . '%',
				// 'Item.isbn_13 LIKE' => '%' . $search . '%'
			)
		);
		
		return $cond;
	}
}
