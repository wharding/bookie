<?php
App::uses('AppModel', 'Model');
/**
 * Upload Model
 *
 * @property Listing $Listing
 * @property Item $Item
 */
class Upload extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'file';

	public $actsAs = array(
		'Upload.Upload' => array( //configurations for upload plugin
			'upload' => array(
                'deleteOnUpdate' => true,
                'thumbnails' => true,
				'thumbnailSizes' => array(
                    'thumb' => '150x150'
                ),
                'thumbnailMethod' => 'imagick',//KU severs have imagick installed
                'mediaThumbnailType' => 'jpg',
                'thumbnailType' => 'jpg',
			)
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Listing' => array(
			'className' => 'Listing',
			'foreignKey' => 'foreign_key',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Item' => array(
			'className' => 'Item',
			'foreignKey' => 'foreign_key',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	
	public $validate = array(
		'file' => array(
			'fileSize' => array(
				'rule' => 'isUnderPhpSizeLimit',
				'message' => 'File is too large'
			),
			//since where not checking above, we don't need this check any more either
			// 'fileType' => array(
				// 'rule'	=> 'isValidMimeType',
				// 'message' => 'File type not supported'
			// ),
			// 'fileExtension' => array(
				// 'rule' => 'isValidExtension',
				// 'message' => 'File extension not supported'
			// ),
			'completedUpload' => array(
				'rule' => 'isCompletedUpload',
        		'message' => 'File was not successfully uploaded'
			),
			'fileSuccessfulyWritten' => array(
		        'rule' => 'isSuccessfulWrite',
		        'message' => 'File was unsuccessfully written to the server'
		    )
		)
	);
    
    public function afterFind($results, $primary = false){
        foreach($results as &$result){
            // hacky call to the Listing afterFind method so that isOwner gets properly set for each linked Listing
            if(isset($result['Listing']['id'])) {
                $listing = $this->Listing->find('first', array('conditions' => array('Listing.id' => $result['Listing']['id'])));
                $result['Listing'] = $listing['Listing'];
			    unset($listing);
            }
		}
		unset($result);
		return $results;
	}
}
