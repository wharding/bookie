<?php
App::uses('AppModel', 'Model');
/**
 * Item Model
 *
 * @property Listing $Listing
 * @property Upload $Upload
 * @property Course $Course
 */
class Item extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';



	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Listing' => array(
			'className' => 'Listing',
			'foreignKey' => 'listing_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Image' => array(
			'className' => 'Upload',
			'foreignKey' => 'foreign_key',
			'dependent' => false,
			'conditions' => array(
				'Image.model' => 'Item'
			),
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Course' => array(
			'className' => 'Course',
			'joinTable' => 'courses_items',
			'foreignKey' => 'item_id',
			'associationForeignKey' => 'course_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

    public function afterFind($results, $primary = false){//the public facing directory for images
		$public_base_dir = '/files/image/upload/';
		//the internal file system location of the image
		$private_base_dir = WWW_ROOT . 'files' . DS . 'image' . DS . 'upload';
		
        foreach($results as &$result){
            // hacky call to the Listing afterFind method so that isOwner gets properly set for each linked Listing
            if(isset($result['Listing']['id'])) {
                $listing_count = $this->Listing->find('count', array('conditions' => array('Listing.id' => $result['Listing']['id'])));
                $result['Listing']['isOwner'] = $listing_count;
                $this->log($listing_count, 'debug');
            }
            
            
            //update the record with the public image reference
			$file_pointer = $private_base_dir . '/' . @$result['Image'][0]['id'] . '/' . 'thumb_' . @$result['Image'][0]['upload'];
			$file_pointer = Folder::correctSlashFor($file_pointer);
			
			$this->log($file_pointer, 'debug');
			
			if(isset($result['Image'][0]) && file_exists($file_pointer)) {
				$this->log('Image exists - Item', 'debug');
				$result['Item']['thumb'] = $public_base_dir . $result['Image'][0]['id'] . '/thumb_' . $result['Image'][0]['upload'];
			} else {
				$result['Item']['thumb'] = '/img/thumb_no_preview.jpg';
			}
            
            $this->log($result, 'debug');
		}
		unset($result);
		return $results;
	}

}
