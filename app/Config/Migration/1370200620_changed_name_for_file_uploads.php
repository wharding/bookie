<?php
class ChangedNameForFileUploads extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 * @access public
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 * @access public
 */
	public $migration = array(
		'up' => array(
			'create_field' => array(
				'uploads' => array(
					'model' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1', 'after' => 'id'),
					'foreign_key' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'after' => 'model'),
					'upload' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 32, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1', 'after' => 'foreign_key'),
					'attachment' => array('type' => 'string', 'null' => false, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1', 'after' => 'upload'),
					'dir' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1', 'after' => 'attachment'),
					'type' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1', 'after' => 'dir'),
					'size' => array('type' => 'integer', 'null' => true, 'default' => '0', 'after' => 'type'),
					'active' => array('type' => 'boolean', 'null' => true, 'default' => '1', 'after' => 'size'),
				),
			),
			'drop_field' => array(
				'uploads' => array('created', 'modified', 'listing_id', 'item_id', 'file', 'file_dir', 'file_type', 'file_size',),
			),
			'alter_field' => array(
				'uploads' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary'),
				),
			),
		),
		'down' => array(
			'drop_field' => array(
				'uploads' => array('model', 'foreign_key', 'upload', 'attachment', 'dir', 'type', 'size', 'active',),
			),
			'create_field' => array(
				'uploads' => array(
					'created' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
					'modified' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
					'listing_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
					'item_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
					'file' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
					'file_dir' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
					'file_type' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 50, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
					'file_size' => array('type' => 'integer', 'null' => true, 'default' => NULL),
				),
			),
			'alter_field' => array(
				'uploads' => array(
					'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
				),
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function after($direction) {
		return true;
	}
}
