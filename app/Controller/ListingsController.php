<?php
App::uses('AppController', 'Controller');
/**
 * Listings Controller
 *
 * @property Listing $Listing
 */
class ListingsController extends AppController {
	public $components = array(
		'Comments.Comments' => array(
			'userModelClass' => 'Users.User' // Customize the User class
		),
		'Paginator',
		'Search.Prg' //enable search
	); 

	public $presetVars = true; //use model for search config
	public $paginate = array();
	
	public $uses = array('Listing', 'Course');
/**
 * beforeFilter method
 *
 * @return void
 */ 
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow();
        if ($this->Auth->user('role') != 'admin') {
            $this->Auth->deny('admin_index', 'admin_view', 'admin_add', 'admin_edit', 'admin_delete');
        }
        if (!$this->Auth->loggedIn()) {
            $this->Auth->deny('add', 'edit', 'delete');
        }             
    }
/**
 * Initializes the view type for comments widget
 *
 * @return string
 * @access public
 */
    public function callback_commentsInitType() {
        return 'tree'; // threaded, tree and flat supported
    }
    
    //TODO: implement after add function to send users email
    
/**
 * index method
 *
 * @return void
 */
	public function index() {
		// $this->Listing->recursive = 0;
		$this->Prg->commonProcess();//setup Search
		$this->paginate['conditions'] = $this->Listing->parseCriteria($this->Prg->parsedParams());
		
		$listings = $this->paginate('Listing', $this->paginate['conditions']);
		$this->set('listings', $listings);
		
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Listing->id = $id;
		if (!$this->Listing->exists()) {
			throw new NotFoundException(__('Invalid listing'));
		}
		$this->set('listing', $this->Listing->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Listing->create();
			if ($this->Listing->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The listing has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing could not be saved. Please, try again.'));
			}
		}
		$users = $this->Listing->User->find('list');
		$courses = $this->Course->find('list');
		$this->set(compact('users','courses'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Listing->id = $id;
		if (!$this->Listing->exists()) {
			throw new NotFoundException(__('Invalid listing'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Listing->save($this->request->data)) {
				$this->Session->setFlash(__('The listing has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Listing->read(null, $id);
		}
		$users = $this->Listing->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Listing->id = $id;
		if (!$this->Listing->exists()) {
			throw new NotFoundException(__('Invalid listing'));
		}
		if ($this->Listing->delete()) {
			$this->Session->setFlash(__('Listing deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Listing was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Listing->recursive = 0;
		$this->set('listings', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Listing->id = $id;
		if (!$this->Listing->exists()) {
			throw new NotFoundException(__('Invalid listing'));
		}
		$this->set('listing', $this->Listing->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Listing->create();
			if ($this->Listing->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The listing has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing could not be saved. Please, try again.'));
			}
		}
		$users = $this->Listing->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Listing->id = $id;
		if (!$this->Listing->exists()) {
			throw new NotFoundException(__('Invalid listing'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Listing->saveAll($this->request->data)) {
				$this->Session->setFlash(__('The listing has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Listing->read(null, $id);
		}
		$users = $this->Listing->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Listing->id = $id;
		if (!$this->Listing->exists()) {
			throw new NotFoundException(__('Invalid listing'));
		}
		if ($this->Listing->delete()) {
			$this->Session->setFlash(__('Listing deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Listing was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
