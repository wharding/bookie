<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Pages
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Debugger', 'Utility');
?>
<h2><?php echo __('Welcome to Bookie!'); ?></h2>
<div class="search listings">
	<h4>Search by listings</h4>
	<?php 
		echo $this->Form->create('Listing',array(
			'url' => array_merge(array('controller' => 'listings', 'action' => 'index'), $this->params['pass']),
			'novalidate' => true
		));
		echo $this->Form->input('search', array('label' => ''));
		
		echo $this->Form->end('Search Listings')
    ?>
</div>

