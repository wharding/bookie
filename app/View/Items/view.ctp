<h2><?php echo $item['Item']['title'] ?></h2>
<div class="item view cf">
	<div class="images">
		<?php foreach($item['Image'] as $index => $image): ?>
		<?php if($index >= 5) break; ?>
		<?php endforeach; ?>
	</div>
	<div class="details">
		<div class="price">
			Price: <?php echo $item['Item']['price'] ?  '$' . $item['Item']['price'] : 'Not specified' ?>
		</div>
		<div class="author">
			Author: <?php echo $item['Item']['author'] ? $item['Item']['author'] : 'Not specified' ?>
		</div>
		<div class="version">
			Version: <?php echo $item['Item']['version'] ? $item['Item']['version'] : 'Not specified' ?>
		</div>
		<div class="isbn_10">
			ISBN 10: <?php echo $item['Item']['isbn_10'] ? $item['Item']['isbn_10'] : 'Not specified' ?>
		</div>
		<div class="isbn_13">
			ISBN 13: <?php echo $item['Item']['isbn_13'] ? $item['Item']['isbn_13'] : 'Not specified' ?>
		</div>
		<div>
		  	<?php echo $this->Html->link('Full Listing', array('controller' => 'listings', 'action' => 'view', $item['Listing']['id'])) ?>
		</div>
		<div class="actions">
			<?php if($item['Listing']['isOwner']): ?>
			    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $item['Item']['id'])); ?>
			    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $item['Item']['id']), null, __('Are you sure you want to delete # %s?', $item['Item']['id'])); ?>
			<?php endif ?>
		</div>
	</div>
	<div class="description">
		Description: <br />
		<?php echo $item['Item']['description'] ? $item['Item']['description'] : 'None provided' ?> 
	</div>
	<div class="created">
		<?php echo $this->Time->nice($item['Item']['created'])?>
	</div>
</div>




<!-- <div class="add_image">
<?php echo $this->Form->create('Upload', array('type' => 'file', 'url' => array('controller' => 'uploads', 'action' => 'add'))); ?>
	<fieldset>
		<legend><?php echo __('Add Image'); ?></legend>
	<?php
		echo $this->Form->input('id', array('type' => 'hidden', 'value' => $item['Item']['id']));
		echo $this->Form->input('upload', array('type' => 'file'));
		echo $this->Form->input('model', array('type' => 'hidden', 'value' => 'Item'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div> -->



	<!--
<div class="related">
		<h3><?php echo __('Related Images'); ?></h3>
	<?php if (!empty($item['Upload'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($item['Upload'] as $upload): ?>
		<tr>
		    <td><?php echo $this->Html->image($item['Listing']['thumb'], array('width' => 50, 'url' => array('controller' => 'listings', 'action' => 'view', $listing['Listing']['id']))) ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'uploads', 'action' => 'view', $upload['id'])); ?>
                <?php if($item['Listing']['isOwner']): ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'uploads', 'action' => 'edit', $upload['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'uploads', 'action' => 'delete', $upload['id']), null, __('Are you sure you want to delete # %s?', $upload['id'])); ?>
			    <?php endif; ?>
            </td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>
    <?php if($item['Listing']['isOwner']): ?>
	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Upload'), array('controller' => 'uploads', 'action' => 'add')); ?> </li>
		</ul>
	</div>
    <?php endif; ?>
</div>
    -->
<div class="related">
	<h3><?php echo __('Related Courses'); ?></h3>
	<?php if (!empty($item['Course'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Department'); ?></th>
		<th><?php echo __('Course Number'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($item['Course'] as $course): ?>
		<tr>
			<td><?php echo $course['id']; ?></td>
			<td><?php echo $course['created']; ?></td>
			<td><?php echo $course['modified']; ?></td>
			<td><?php echo $course['department']; ?></td>
			<td><?php echo $course['course_number']; ?></td>
			<td><?php echo $course['name']; ?></td>
			<td><?php echo $course['description']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'courses', 'action' => 'view', $course['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'courses', 'action' => 'edit', $course['id'])); ?>
				<?php if($request['isAdmin']) echo $this->Form->postLink(__('Delete'), array('controller' => 'courses', 'action' => 'delete', $course['id']), null, __('Are you sure you want to delete # %s?', $course['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Course'), array('controller' => 'courses', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
