<div class="items index">
	<h2>Books</h2>
	<div id="sort_by">
	  Sort by: 
		<ul class="sort_list">
			<li><?php echo $this->Paginator->sort('price'); ?></li>
			<li><?php echo $this->Paginator->sort('user_id'); ?></li>
			<li><?php echo $this->Paginator->sort('title'); ?></li>
			<li><?php echo $this->Paginator->sort('section'); ?></li>
			<li><?php echo $this->Paginator->sort('created'); ?></li>
		</ul>
	</div>
</div>
<div class="results cf">
	<?php foreach($items as $index => $item): ?>
		<div class="item result cf">
			<h3>
				<?php 
					$title = $item['Item']['title'] ? $item['Item']['title'] : 'Unknown';
					$title = $this->Text->truncate($title, 20);
					echo $this->Html->link($title, array('action' => 'view', $item['Item']['id']));
				?>
			</h3>
			<div class="image">
				<?php //echo $this->Html->image($item['Item']['thumb'], array('url' => array('controller' => 'items', 'action' => 'view', $item['Item']['id']))) ?>
			</div>
			<div class="details">
				<div class="author">
					Author: <?php echo $item['Item']['author'] ? $item['Item']['author'] : 'Not specified' ?>
				</div>
				<div class="isbn_10">
					ISBN 10: <?php echo $item['Item']['isbn_10'] ? $item['Item']['isbn_10'] : 'Not specified' ?>
				</div>
				<div class="isbn_13">
					ISBN 13: <?php echo $item['Item']['isbn_13'] ? $item['Item']['isbn_13'] : 'Not specified' ?>
				</div>
				<div class="price">
					Price: <?php echo $item['Item']['price'] ?  '$' . $item['Item']['price'] : 'Not specified' ?>
				</div>
			</div>
			Description:
			<div class="description">
				<?php echo $item['Item']['description'] ? $this->Text->truncate($item['Item']['description'], 200) : 'None Provided' ?>
			</div>
			<div class="created">
				<?php echo $this->Time->nice($item['Item']['created'])?>
			</div>
		</div>
	<?php endforeach; ?>
</div>
<div>
	<?php
		echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
		));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		var max_height = 0;
		//lets make all of the boxes the same height
		$('.item.result').each(function(index){
			var height = $(this).height();
			if(height > max_height) {
				max_height = height;
			}
		}).each(function(){
			$(this).css('min-height', max_height);
		});
	});
	
</script>
