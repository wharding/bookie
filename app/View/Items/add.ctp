<div class="items form">
<?php echo $this->Form->create('Item', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Add Item'); ?></legend>
	<?php
		echo $this->Form->input('listing_id', array('type' => 'hidden', 'value' => $listing_id));
		echo $this->Form->input('title');
		echo $this->Form->input('author');
		echo $this->Form->input('description');
		echo $this->Form->input('version');
		echo $this->Form->input('isbn_10');
		echo $this->Form->input('isbn_13');
		echo $this->Form->input('price', array('value' => '0.00'));
		echo $this->Form->input('Course');
		// echo $this->Form->input('Image.upload.upload', array('type' => 'file'));
		// echo $this->Form->input('Image.upload.model', array('type' => 'hidden', 'value' => 'Item'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
