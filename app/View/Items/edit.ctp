<?php if($this->Form->value('Listing.isOwner')): ?>
<div class="">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Item.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Item.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Uploads'), array('controller' => 'uploads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Upload'), array('controller' => 'uploads', 'action' => 'add')); ?> </li>
	</ul>
</div>
<?php endif; ?>
<div class="items form">
<?php echo $this->Form->create('Item', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Edit Item'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('listing_id');
		echo $this->Form->input('title');
		echo $this->Form->input('author');
		echo $this->Form->input('description');
		echo $this->Form->input('version');
		echo $this->Form->input('isbn_10');
		echo $this->Form->input('isbn_13');
		echo $this->Form->input('price');
		echo $this->Form->input('Course');
		
		// echo $this->Form->input('Image.upload.upload', array('type' => 'file'));
        // echo $this->Form->input('Image.upload.model', array('type' => 'hidden', 'value' => 'Item'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
