<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$bookie = 'Bookie';
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this -> Html -> charset(); ?>
	<title>
		<?php echo $bookie; ?>
	</title>
	<?php
	echo $this -> Html -> meta('icon');

	echo $this -> Html -> css('cake.generic');
	echo $this -> Html -> script('jquery');
	//echo $this -> Html -> css('bootstrap');
	//echo $this -> Html -> css('bootstrap-responsive');

	echo $this -> fetch('meta');
	echo $this -> fetch('css');
	echo $this -> fetch('script');
	?>
</head>
<body>
	<div id="container">
		<div id="header">
			<div id="header_content">
				
				<ul class="nav_area">
					<li><?php echo $this -> Html -> link(__('Home'), array('admin' => false, 'plugin' => null, 'controller' => 'pages', 'action' => 'home'), array('class' => 'nava')); ?></li>
					<li><?php echo $this -> Html -> link(__('Listings'), array('admin' => false, 'plugin' => null, 'controller' => 'listings', 'action' => 'index'), array('class' => 'nava')) ?></li>
					<?php if($request['isLoggedIn']): ?>
						<li><?php echo $this -> Html -> link(__('New Listing'), array('admin' => false, 'plugin' => null, 'controller' => 'listings', 'action' => 'add'), array('class' => 'nava')) ?></li>
					<?php endif; ?>
					<li><?php echo $this -> Html -> link(__('Books'), array('admin' => false, 'plugin' => null, 'controller' => 'items', 'action' => 'index'), array('class' => 'nava')) ?></li>
					<li><?php echo $this -> Html -> link(__('Courses'), array('admin' => false, 'plugin' => null, 'controller' => 'courses', 'action' => 'index'), array('class' => 'nava')) ?></li>
									
				</ul>
				
				
	            <ul class="user_options">
	            	<li>
			            <?php if($request['isLoggedIn']): ?>
						    <?php echo $this->Html->link('Log out', array('admin' => false, 'controller' => 'users', 'action' => 'logout'), array('class' => 'log')); ?>
						<?php else: ?>
						    <?php echo $this->Html->link('Log in', array('admin' => false, 'controller' => 'users', 'action' => 'login'), array('class' => 'log')); ?>
						<?php endif; ?>
	            	</li>
		        </ul>
			</div>
		</div>
		<div id="content">
			<?php echo $this -> Session -> flash(); ?>

			<?php echo $this -> fetch('content'); ?>
		</div>
		<div id="footer">
		</div>
	</div>
</body>
</html>
