<div class="listings index">
	<h2><?php echo __('Listings'); ?></h2>
	<div id="sort_by">
	  Sort by: 
		<ul class="sort_list">
			<li><?php echo $this->Paginator->sort('price'); ?></li>
			<li><?php echo $this->Paginator->sort('user_id'); ?></li>
			<li><?php echo $this->Paginator->sort('title'); ?></li>
			<li><?php echo $this->Paginator->sort('section'); ?></li>
			<li><?php echo $this->Paginator->sort('created'); ?></li>
		</ul>
	</div>
	<div class="results">
		<?php foreach ($listings as $listing): ?>
			<div class="hr"></div>
			<div class="listing result cf">
				<h3><?php echo $this->Html->link($listing['Listing']['title'], array('action' => 'view', $listing['Listing']['id']) )?></h3>
				<div class="result_content">
					<div class="overview">
						<div class="image">
							<?php echo $this->Html->image($listing['Listing']['thumb'], array('url' => array('controller' => 'listings', 'action' => 'view', $listing['Listing']['id']))) ?>
						</div>
						<div class="details">
							<div class="price">
								Price: $<?php echo $listing['Listing']['price']?>
							</div>
							<div class="section">
								Section: <?php echo ($listing['Listing']['section'] ? 'B' : 'A') ?>
							</div>
							<div class="user">
								Sold by: <?php echo $listing['User']['username']?>
							</div>
						</div>
						<div class="actions">
							<?php echo $this->Html->link(__('View'), array('action' => 'view', $listing['Listing']['id'])); ?>
							<?php if($listing['Listing']['isOwner']): ?>
							    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $listing['Listing']['id'])); ?>
							    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $listing['Listing']['id']), null, __('Are you sure you want to delete %s?', $listing['Listing']['title'])); ?>
							<?php endif ?>
						</div>
					</div>
					<div class="description">
						<?php echo ($listing['Listing']['description'] ? $listing['Listing']['description'] : 'No Description Provided') ?>
					</div>
					<div class="created">
						<?php echo $this->Time->nice($listing['Listing']['created']) ?>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
