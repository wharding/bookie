<div class="listings form">
<?php echo $this->Form->create('Listing', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Edit Listing'); ?></legend>
	<?php
		echo $this->Form->input('user_id', array('type' => 'hidden', 'value' => $user_info['id']));
		echo $this->Form->input('title');
		echo $this->Form->input('price', array('step' => '0.01'));
		echo $this->Form->input('description');
		echo $this->Form->input('section', array(
			'type' => 'select',
			'options' => array('A', 'B'),
			'empty' => '(Choose section)'
		));
        echo $this->Form->input('Image.upload.upload', array('type' => 'file'));
        echo $this->Form->input('Image.upload.model', array('type' => 'hidden', 'value' => 'Listing'));
		/*
        echo $this->Form->input('id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('title');
		echo $this->Form->input('price');
		echo $this->Form->input('description');
		echo $this->Form->input('section');
        */
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>

