<h2><?php echo $listing['Listing']['title'] ?></h2>
<div class="listing cf">
	<div class="listing_content">
		<div class="overview">
			<div class="image">
				<?php echo $this->Html->image($listing['Listing']['thumb'], array('url' => array('controller' => 'listings', 'action' => 'view', $listing['Listing']['id']))) ?>
			</div>
			<div class="details">
				<div class="price">
					Price: $<?php echo $listing['Listing']['price']?>
				</div>
				<div class="section">
					Section: <?php echo ($listing['Listing']['section'] ? 'B' : 'A') ?>
				</div>
				<div class="user">
					Sold by: <?php echo $listing['User']['username']?>
				</div>
			</div>
			<div class="actions">
				<?php if($listing['Listing']['isOwner']): ?>
				    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $listing['Listing']['id'])); ?>
				    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $listing['Listing']['id']), null, __('Are you sure you want to delete # %s?', $listing['Listing']['id'])); ?>
				<?php endif ?>
			</div>
		</div>
		<div class="description">
			<?php echo ($listing['Listing']['description'] ? $listing['Listing']['description'] : 'No Description Provided') ?>
		</div>
		<div class="created">
			<?php echo $listing['Listing']['created']?>
		</div>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Books'); ?></h3>
	<?php if (!empty($listing['Item'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<!--th><?php echo __('Id'); ?></th-->
		<!-- <th></th> -->
		<!--th><?php echo __('Listing Id'); ?></th-->
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Author'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Version'); ?></th>
		<th><?php echo __('Isbn 10'); ?></th>
		<th><?php echo __('Isbn 13'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($listing['Item'] as $key=>$item): ?>
		<tr>
			<!--td><?php echo $item['id']; ?></td-->
			<!-- <td><?php echo $this->Html->image($listing['Item'][$key]['thumb'], array('width' => 50, 'url' => array('controller' => 'items', 'action' => 'view', $listing['Item'][$key]['id']))) ?></td> -->
			<!--td><?php echo $item['listing_id']; ?></td-->
			<td><?php echo $item['title']; ?></td>
			<td><?php echo $item['author']; ?></td>
			<td><?php echo $this->Text->truncate($item['description'], 300); ?></td>
			<td><?php echo $item['version']; ?></td>
			<td><?php echo $item['isbn_10']; ?></td>
			<td><?php echo $item['isbn_13']; ?></td>
			<td><?php echo $item['price'] ? $item['price']	: "" ; ?></td>
            <td class="actions">
                <?php 
				    echo $this->Html->link(__('View'), array('controller' => 'items', 'action' => 'view', $item['id']));
                    //Since only listing owners can create new items, only listing owners could edit or delete them
                    if($listing['Listing']['isOwner']) {
                        echo $this->Html->link(__('Edit'), array('controller' => 'items', 'action' => 'edit', $item['id']));
				        echo $this->Form->postLink(__('Delete'), array('controller' => 'items', 'action' => 'delete', $item['id']), null, __('Are you sure you want to delete # %s?', $item['id']));
                    }
                ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<?php if($listing['Listing']['isOwner']): ?>
    <div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('Add another book'), array('controller' => 'items', 'action' => 'add', $listing['Listing']['id'])); ?> </li>
		</ul>
	</div>
    <?php endif; ?>
</div>

<div id="listing-comments">
	<?php $this->CommentWidget->options(array('allowAnonymousComment' => false));?>
	<?php echo $this->CommentWidget->display();?>
</div>