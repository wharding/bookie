<div class="listings form">
<?php echo $this->Form->create('Listing', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Add Listing'); ?></legend>
	<?php
		echo $this->Form->input('user_id', array('type' => 'hidden', 'value' => $user_info['id']));
		echo $this->Form->input('title');
		echo $this->Form->input('price', array('step' => '1', 'value' => '0.00'));
		echo $this->Form->input('description');
		echo $this->Form->input('section', array(
			'type' => 'select',
			'options' => array('A', 'B', 'Other'),
			'empty' => '(Choose section)'
		));
		echo $this->Form->input('Image.upload.upload', array('type' => 'file', 'label' => 'Add an image for this listing'));
		echo $this->Form->input('Image.upload.model', array('type' => 'hidden', 'value' => 'Listing'));
		
		
	?>
	</fieldset>
	<fieldset>
	  <legend>Add a book to the listing</legend>
	  <p>Don't worry, you can alway add more later</p>
			 <?php
				echo $this->Form->input('Item.0.title');
				echo $this->Form->input('Item.0.author');
				echo $this->Form->input('Item.0.description');
				echo $this->Form->input('Item.0.version');
				echo $this->Form->input('Item.0.isbn_10');
				echo $this->Form->input('Item.0.isbn_13');
				echo $this->Form->input('Item.0.price', array('label' => 'Individual Price'));
				echo $this->Form->input('Item.0.Course', array('type' => 'select', 'label' => 'Course(s)', 'multiple' => true,
					'options' => $courses
				));
			 ?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>

