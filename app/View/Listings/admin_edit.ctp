<div class="">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Listing.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Listing.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Listings'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Items'), array('controller' => 'items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Item'), array('controller' => 'items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Uploads'), array('controller' => 'uploads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Upload'), array('controller' => 'uploads', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="listings form">
<?php echo $this->Form->create('Listing'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Listing'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('title');
		echo $this->Form->input('price');
		echo $this->Form->input('description');
		echo $this->Form->input('section');
		
		echo $this->Form->input('Image.upload.upload', array('type' => 'file'));
        echo $this->Form->input('Image.upload.model', array('type' => 'hidden', 'value' => 'Listing'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>

