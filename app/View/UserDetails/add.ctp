<div class="userDetails form">
<?php echo $this->Form->create('UserDetail'); ?>
	<fieldset>
		<legend><?php echo __('Add User Detail'); ?></legend>
	<?php
		echo $this->Form->input('user_id');
		echo $this->Form->input('position');
		echo $this->Form->input('field');
		echo $this->Form->input('value');
		echo $this->Form->input('input');
		echo $this->Form->input('data_type');
		echo $this->Form->input('label');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List User Details'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
